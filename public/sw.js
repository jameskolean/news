// eslint-disable-next-line no-undef
importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.6.1/workbox-sw.js');

const staticAssets = [
  { url: './' },
  { url: './app.js' },
  { url: './styles.css' },
  { url: './fallback.json' },
  { url: './images/fetch-dog.jpg' },
];

workbox.precaching.precacheAndRoute(staticAssets)

workbox.routing.registerRoute(
  new RegExp('https://newsapi.org/.*'),
  workbox.strategies.networkFirst()
)

workbox.routing.registerRoute(
  new RegExp('.*\.(png|jpg|jpeg|gif)'),
  workbox.strategies.cacheFirst({
    cacheName: 'image-cache',
    plugins: [
      new workbox.expiration.Plugin({
        // Only cache requests for a week
        maxAgeSeconds: 7 * 24 * 60 * 60,
        // Only cache 10 requests.
        maxEntries: 20,
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ]
  })
);
workbox.routing.setCatchHandler(({url, event, params}) => {
  console.log('In setCatchHandler')
});