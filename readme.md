# Simple PWA example 

Online demo can be found on https://jameskolean.github.io/news

## Running locally

Serve folder with your server of choice. For instance `npm install -g serve`.

## Add PWA to Chrome Desktop 
    App Banners: chrome://flags#enable-app-banners
    Experimental App Banners: chrome://flags#enable-experimental-app-banners
    Desktop PWAs: chrome://flags#enable-desktop-pwas
